import argparse
from dataclasses import dataclass

import networkx as nx
import osmnx as ox
import pandas as pd
from pandarallel import pandarallel

pandarallel.initialize()


@dataclass
class Point:
    x: float
    y: float


def get_osm_route(G, point1: Point, point2: Point):

    try:
        route = nx.shortest_path(G, point1, point2, weight="length")
    except nx.NetworkXNoPath:
        route = []

    return route


parser = argparse.ArgumentParser()
parser.add_argument(
    "--source_path", help="Path to source file with coordinates", type=str)
parser.add_argument("--target_path", help="Path to write the result", type=str)


if __name__ == "__main__":

    args = parser.parse_args()
    data = pd.read_csv(args.source_path)
    print("Data collected")

    map = "New York, USA"

    G = ox.graph_from_place((map), network_type="drive")
    G = ox.add_edge_speeds(G)
    G = ox.add_edge_travel_times(G)

    data["pickup_node"] = ox.nearest_nodes(
        G, data["pickup_longitude"].to_list(), data["pickup_latitude"].to_list())

    data["dropoff_node"] = ox.nearest_nodes(
        G, data["dropoff_longitude"].to_list(), data["dropoff_latitude"].to_list())

    print("Nodes calculated")

    data["osm_route"] = data.parallel_apply(
        lambda row: get_osm_route(G=G,
                                  point1=row["pickup_node"],
                                  point2=row["dropoff_node"],
                                  ),
        axis=1
    )

    print(data["osm_route"].head(30))

    print("Routes calculated")

    data["osm_distance"] = data.parallel_apply(
        lambda row: nx.path_weight(G, row["osm_route"], weight="length"),
        axis=1
    )

    print("Distances calculated")

    data["osm_route"] = (data["osm_route"].astype("str")
                         .str.replace(",", "")
                         .str[1:-1])

    print("Route converted to str")

    columns = ["id", "osm_distance", "osm_route",
               "pickup_node", "pickup_latitude", "pickup_longitude",
               "dropoff_node", "dropoff_latitude", "dropoff_longitude"]

    data[columns].to_csv(args.target_path, index=False)
