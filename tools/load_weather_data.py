import argparse
import os
import time

from selenium import webdriver
from selenium.webdriver.common import action_chains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

parser = argparse.ArgumentParser()
parser.add_argument(
    "--start_date", help="Start date in dd.mm.yyyy format", type=str)
parser.add_argument("--end_date", help="End date dd.mm.yyyy format", type=str)
parser.add_argument(
    "--target_path", help="Folder to save the result", type=str)


if __name__ == "__main__":

    args = parser.parse_args()

    # setting special options of chrome browser
    chromeOptions = webdriver.ChromeOptions()

    # specifying directory for files download (default is /home/user/Downloads)
    prefs = {"download.default_directory": args.target_path}
    chromeOptions.add_experimental_option("prefs", prefs)

    # driver = webdriver.Chrome(chrome_options=chromeOptions)
    driver = webdriver.Chrome(ChromeDriverManager().install(),
                              chrome_options=chromeOptions)

    driver.get("https://rp5.ru/Weather_archive_in_New_York,_La_Guardia_(airport)")
    action = action_chains.ActionChains(driver)

    download_archive_button = driver.find_element_by_id('tabSynopDLoad')
    download_archive_button.click()

    start_date_field = driver.find_element_by_id('calender_dload')
    start_date_field.clear()
    start_date_field.send_keys(args.start_date)

    end_date_field = driver.find_element_by_id("calender_dload2")
    end_date_field.clear()
    end_date_field.send_keys(args.end_date)

    driver.execute_script("document.getElementById('format2').click();")
    driver.execute_script("document.getElementById('coding2').click();")

    create_gz_button = driver.find_element_by_xpath(
        "//*[contains(text(), 'Select to file GZ (archive)')]")
    create_gz_button.click()

    download_ref = (WebDriverWait(driver, 15)
                    .until(EC.element_to_be_clickable((By.LINK_TEXT, "Download"))))

    download_ref.click()
    filename = download_ref.get_attribute("href").split("/")[-1]

    # Wait until the file is loaded
    while not os.path.exists(os.path.join(args.target_path, filename)):
        time.sleep(0.1)

    # When the downloading is completed change the name
    initial_path = os.path.join(args.target_path, filename)
    destination_path = os.path.join(args.target_path, "rp5_weather.csv.gz")
    os.rename(initial_path, destination_path)

    driver.stop_client()
