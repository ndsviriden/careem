import numpy as np
import pandas as pd

from tools.feature_extractors.abc_extractor import AbcExtractor

AVG_EARTH_RADIUS = 6369


class GeoExtractor(AbcExtractor):

    @staticmethod
    def _get_directions(lat1: np.ndarray, lng1: np.ndarray, lat2: np.ndarray, lng2: np.ndarray) -> np.ndarray:
        # https://www.igismap.com/formula-to-find-bearing-or-heading-angle-between-two-points-latitude-longitude/
        # https://www.igismap.com/map-tool/bearing-angle
        lng_delta_rad = np.radians(lng2 - lng1)
        lat1, lng1, lat2, lng2 = map(np.radians, (lat1, lng1, lat2, lng2))
        y = np.sin(lng_delta_rad) * np.cos(lat2)
        x = (np.cos(lat1) * np.sin(lat2)
             - np.sin(lat1) * np.cos(lat2) * np.cos(lng_delta_rad))

        return np.degrees(np.arctan2(y, x))

    @staticmethod
    def _get_haversine_distance(lat1: np.ndarray, lng1: np.ndarray, lat2: np.ndarray, lng2: np.ndarray) -> np.ndarray:
        lat1, lng1, lat2, lng2 = map(np.radians, (lat1, lng1, lat2, lng2))
        lng_delta_rad = lng2 - lng1
        lat_delta_rad = lat2 - lat1

        a = (np.sin(lat_delta_rad / 2) ** 2
             + np.cos(lat1) * np.cos(lat2) * np.sin(lng_delta_rad / 2) ** 2)

        haversine = 2 * np.arcsin(np.sqrt(a)) * AVG_EARTH_RADIUS

        return haversine

    def extract_features(self, df: pd.DataFrame) -> pd.DataFrame:

        df["geo_direction"] = self._get_directions(
            df["pickup_latitude"].values, df["pickup_longitude"].values,
            df["dropoff_latitude"].values, df["dropoff_longitude"].values)

        df["geo_haversine"] = self._get_haversine_distance(
            df["pickup_latitude"].values, df["pickup_longitude"].values,
            df["dropoff_latitude"].values, df["dropoff_longitude"].values)

        return df
