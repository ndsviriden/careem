from datetime import datetime

import holidays
import pandas as pd

from tools.feature_extractors.abc_extractor import AbcExtractor

us_holidays = holidays.US()


class DatetimeExtractor(AbcExtractor):

    def extract_features(self, df: pd.DataFrame) -> pd.DataFrame:

        # Days
        df["dt_holiday"] = df["pickup_datetime"].apply(
            lambda x: x in us_holidays)
        df["dt_weekday"] = df["pickup_datetime"].dt.weekday
        df["dt_weekend"] = df["dt_weekday"] > 4

        # Hours
        df["dt_hour"] = df["pickup_datetime"].dt.hour
        df["dt_week_hour"] = df["dt_weekday"] * 24 + df["dt_hour"]

        # Minutes
        min_date = datetime(2016, 1, 1)
        df["dt_day_minutes"] = df["dt_hour"] * \
            60 + df["pickup_datetime"].dt.minute
        df["dt_total_minutes"] = (
            df["pickup_datetime"] - min_date).dt.total_seconds() // 60

        return df
