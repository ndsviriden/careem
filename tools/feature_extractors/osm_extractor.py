from typing import List

import numpy as np
import osmnx as ox
import pandas as pd
from tools.feature_extractors.abc_extractor import AbcExtractor

from tqdm import tqdm
G = ox.graph_from_place(("New York, USA"), network_type="drive")
G = ox.add_edge_speeds(G)
G = ox.add_edge_travel_times(G)


class OsmExtractor(AbcExtractor):
    def __init__(self,
                 osm_dataset: str = "../data/train_osm.csv"):

        self.osm_dataset = osm_dataset

    @staticmethod
    def _get_crossroads_avg(route: List[str]) -> float:

        crossroads_avg = np.mean(
            list(
                map(lambda x: len(G[int(x)]._atlas), route)
            )
        )

        return crossroads_avg

    def extract_features(self, df: pd.DataFrame) -> pd.DataFrame:
        # Not in pandas because of memory issues
        with open(self.osm_dataset) as f:
            trip_lengths = []
            trip_avg_crs = []
            trip_ids = []
            # skip header
            next(f)
            for line in tqdm(f):
                if not line:
                    continue
                trip_info = line.split(",")
                trip_id = trip_info[0]
                trip = trip_info[2].split()
                trip_ids.append(trip_id)
                trip_lengths.append(len(trip))
                trip_avg_crs.append(self._get_crossroads_avg(trip))

        osm = pd.read_csv(self.osm_dataset)[
            ["id", "osm_distance"]]

        osm_lengths = pd.DataFrame.from_dict(
            {"id": trip_ids, "osm_n_nodes": trip_lengths, "osm_avg_crossroads": trip_avg_crs})
        osm = pd.merge(osm, osm_lengths, on="id")

        return pd.merge(df, osm, on="id")
