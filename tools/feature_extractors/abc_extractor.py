from abc import ABC, abstractmethod

import pandas as pd


class AbcExtractor(ABC):

    @abstractmethod
    def extract_features(self, df: pd.DataFrame) -> pd.DataFrame:
        pass
