from datetime import datetime

import pandas as pd
from sklearn.preprocessing import LabelEncoder

from tools.feature_extractors.abc_extractor import AbcExtractor


class WeatherExtractor(AbcExtractor):
    def __init__(self,
                 weather_dataset: str = "../data/rp5_weather.csv.gz"):

        self.weather_dataset = weather_dataset

    def _get_weather_data(self, start_dt: datetime, end_dt: datetime) -> pd.DataFrame:

        # Load NYC weather data
        weather = pd.read_csv(self.weather_dataset, sep=';',
                              skiprows=6, index_col=False)
        weather = weather.rename(columns={weather.columns[0]: "dt"})
        weather["dt"] = pd.to_datetime(weather["dt"])

        # Encode text columns
        le = LabelEncoder()
        text_cols = weather.dtypes[weather.dtypes == "object"].index.to_list()
        weather[text_cols] = weather[text_cols].apply(
            lambda col: le.fit_transform(col))

        # Transform raw weather data with 3 hour period into data with 1 hour period
        date_range = pd.DataFrame(pd.date_range(start=start_dt, end=end_dt, freq='1h'),
                                  columns=["dt"],
                                  )
        weather = pd.merge(date_range, weather, on="dt", how="left")

        # Fill missing values via interpolation
        feature_columns = [col for col in weather.columns if col != "dt"]
        weather[feature_columns] = (weather[feature_columns]
                                    .apply(lambda col:
                                           col.interpolate(method="nearest", limit=3))
                                    )

        # Set weather_ prefix to each columns
        rename_dict = {col: f"weather_{col}".lower()
                       for col in feature_columns}
        weather = weather.rename(columns=rename_dict)

        return weather

    def extract_features(self, df: pd.DataFrame) -> pd.DataFrame:

        # Load weather dataset
        weather = self._get_weather_data(start_dt=df["pickup_datetime"].dt.date.min(),
                                         end_dt=df["pickup_datetime"].dt.date.max())

        # Create join key
        df["dt"] = df["pickup_datetime"].apply(
            lambda x: datetime(x.year, x.month, x.day, x.hour))

        return pd.merge(df, weather, on="dt", how="left").drop(columns=["dt"])
