# NY City Taxi Trip Duration Solution

Competition: https://www.kaggle.com/competitions/nyc-taxi-trip-duration

## Project structure

**EDA**: notebooks/eda.ipynb  
**Model**: notebooks/solution.ipynb  
**OSM Loader**: tools/load_osm_data.py  
**Weather Loader**: tools/load_weather_data.py  
**Feature Extractors**: tools/feature_extractors.py  


## Short developer guide

1. Install dependencies from requirements.txt
    ```bash
    pip install -r requirements.txt
    ```

2. Download data.  
    a. Easiest way is to download data.tar.gz and unzip it in careem folder:
    [https://drive.google.com/data.tar.gz](https://drive.google.com/file/d/19cCzxrFkLrs3h4HfsWZXkxZn-VU_ishh/view?usp=sharing)  
    
    b. Or you may download data with the following steps (takes some time):

    * Get OSM data (~3 hours)
    ```bash
    ./run.sh osm train && ./run osm test
    ```
    * Get weather data (installed Chrome is required for it)
    ```bash
    ./run.sh weather
    ```
    * Download data (competitions data and OSM features cache) into data folder:
    ```bash
    ./run.sh download
    ```
    
3. Run tests
    ```bash
    ./run.sh tests
    ```

## Sources
* https://osmnx.readthedocs.io/en/stable/
* https://rp5.ru/Weather_in_New_York,_USA
