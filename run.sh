#!/bin/bash

case "$1" in
  "download")
    wget "https://drive.google.com/u/1/uc?id=19cCzxrFkLrs3h4HfsWZXkxZn-VU_ishh&export=download&confirm=t"
    tar -xvzf data.tar.gz -C data_test
    ;;
  "osm")
    case "$2" in 
      "train")
        python tools/load_osm_data.py --source_path="data/train.zip" --target_path="data/train_osm.csv"
        ;;
      "test")
        python tools/load_osm_data.py --source_path="data/test.zip" --target_path="data/test_osm.csv"
        ;;
       *)
       "$0" -h
       ;;
    esac
    ;;
  "weather")
    python tools/load_weather_data.py --start_date="01.01.2016" --end_date="01.07.2016" --target_path="${PWD}/data"
    ;;
  "tests")
    python -m unittest discover tests/
    ;;
  "-h")
    echo "Usage:
    ./run tests -- for run tests
    ./run osm train -- for calculating train osm features (takes much time)
    ./run osm test -- for calculating test osm features (takes much time)
    ./run -h --for help"
    ;;
  *)
    "$0" -h
    ;;
esac