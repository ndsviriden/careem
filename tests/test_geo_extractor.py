from dataclasses import dataclass
import unittest

import numpy as np
from tools.feature_extractors.geo_extractor import GeoExtractor


@dataclass
class Point:
    lat: float
    long: float


class TestGeoExtractor(unittest.TestCase):

    def setUp(self) -> None:
        geo_extractor = GeoExtractor()
        self.geo_extractor = geo_extractor
        return super().setUp()

    @staticmethod
    def get_data():
        pickup_points = [
            Point(39.10, -94.58),
            Point(42, -72)
        ]
        dropoff_points = [
            Point(38.63, -90.20),
            Point(45, -68)
        ]
        pickup_lats = np.array([p.lat for p in pickup_points])
        pickup_longs = np.array([p.long for p in pickup_points])
        dropoff_lats = np.array([p.lat for p in dropoff_points])
        dropoff_longs = np.array([p.long for p in dropoff_points])
        return pickup_lats, pickup_longs, dropoff_lats, dropoff_longs

    def test_direction(self):
        pickup_lats, pickup_longs, dropoff_lats, dropoff_longs = self.get_data()

        directions = np.array([96.468, 42.681])

        pred_directions = self.geo_extractor._get_directions(
            pickup_lats, pickup_longs, dropoff_lats, dropoff_longs)
        np.testing.assert_almost_equal(pred_directions, directions, decimal=2)

    def test_haversine_distance(self):
        pickup_lats, pickup_longs, dropoff_lats, dropoff_longs = self.get_data()
        distances = np.array([382.76, 463.96])
        pred_directions = self.geo_extractor._get_haversine_distance(
            pickup_lats, pickup_longs, dropoff_lats, dropoff_longs)
        np.testing.assert_almost_equal(pred_directions, distances, decimal=0)
